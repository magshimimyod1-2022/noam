import socket
from datetime import date, timedelta


def check_weather(city, date):
    """
    Function checks the weather of a certain city in a certain
    date by sending a request to the weather client and extracting
    the info.
    :param city: city to check weather of
    :param date: for the weather of a certain date
    :return: tuple with the temperature and weather description
    """
    info_tuple = ()
    SERVER_IP = "34.218.16.79"
    SERVER_PORT = 77
    gematria_table = {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8, "i": 9, "j": 10, "k": 11, "l": 12,
                "m": 13,
                "n": 14, "o": 15, "p": 16, "q": 17, "r": 18, "s": 19, "t": 20, "u": 21, "v": 22, "w": 23, "x": 24,
                "y": 25, "z": 26, " ": 0}

    # Checksum calculation
    checksum_part1 = 0
    for letter in city:
        checksum_part1 += gematria_table[letter.lower()]

    # Checksum calculation (sum of digits of full date)
    checksum_part2 = 0
    for element in date.split("/"):
        for dig in element:
            checksum_part2 += int(dig)

    server_address = (SERVER_IP, SERVER_PORT)
    message = "100:REQUEST:city={}&date={}&checksum={}.{}".format(city, date, checksum_part1, checksum_part2)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect(server_address)
        sock.sendall(message.encode())
        response = sock.recv(1024)
        response = sock.recv(1024)

    response = response.decode()
    response = response.split(":")
    data = response[2]
    # Checking the validity of the request
    if response[1] == "ERROR":
        info_tuple += "999", response[2],
    else:
        data = data.split("&")
        # Adding the weather description and temperature
        info_tuple += float(data[2].split("=")[1]), data[3].split("=")[1]
    return info_tuple


def hottest_countries(file_path):
    city_dict = {}
    with open(file_path, "r") as file:
        for element in file:
            line = element.split(",")
            today = date.today()
            today = today.strftime("%d/%m/%Y")
            city_dict[line[2]] = check_weather(line[2], today)[0]
    return city_dict

def main():
    """
    Function receives info from user and presents them with options
    and performs the actions based on the user's choice
    :return: none.
    """
    city = input("Where do you live mate eh? (*in british accent*)\n")
    choice = int(input("OPTIONS:\n1. What's the weather today eh?\n2. Next 3 days\n"))
    if choice == 1:
        today = date.today()
        today = today.strftime("%d/%m/%Y")  # Extracting the date string
        print("Today's weather is: {}".format(check_weather(city, today)))
    elif choice == 2:
        for i in range(4):
            # Creating a date the function can properly process
            day = date.today() + timedelta(days=i)
            day = str(day).replace("-", "/").split("/")
            day.reverse()
            day = "/".join(day)
            print(day + ", Temperature: %.2f, %s." % check_weather(city, day))
    else:
        print("INVALID CHOICE! (Valid options: 1, 2)")


#if __name__ == "__main__":
#    main()
print(hottest_countries(r"c:\temp\capitals20.txt"))