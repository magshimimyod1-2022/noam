import socket

HOST = '127.0.0.1'
PORT = 65432
# FAKE DATA CONSTANTS: (FOR PART 1 OF PROJECT)
ALBUMS = "IGOR,THANK YOU NEXT,WHEN WE ALL FALL ASLEEP,DAMN.,KIDS SEE GHOSTS"
LYRICS = "Never gonna give you up, never gonna let you down, never run around and desert you"
SONGS_IN_ALBUM = "Happy,Sad,Mad,Upset,Angry"
NUM_SONGS_IN_ALBUM = 6
LENGTH = "4:21"
ALBUM_OF_SONG = "IGOR"
FIND_SONG_BY_NAME = "THANK YOU NEXT,KIDS SEE GHOSTS" # In response to "K"
FIND_SONG_BY_LYRICS = "IGOR,KIDS SEE GHOSTS"

# PROTOCOL CODES:
OK_CODE = "200"
REQUEST_ALBUM_LIST = "100"
REQUEST_ALBUM_SONGS = "200"
REQUEST_SONG_LENGTH = "300"
REQUEST_SONG_LYRICS = "400"
REQUEST_SONG_ALBUM = "500"
REQUEST_SONG_BY_NAME = "600"
REQUEST_SONG_BY_LYRICS = "700"
QUIT = "800"


def main():
    while True:
        message = "Welcome"
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
            # Connection:
            server_address = ('', PORT)
            server.bind(server_address)
            server.listen(1)
            client_soc, client_address = server.accept()

            # Communication:
            client_msg = client_soc.recv(1024)
            client_soc.sendall(message.encode())
            client_msg = client_soc.recv(1024)
            client_msg = client_msg.decode()

            # Request handling:
            request_code = client_msg.split(":")[0]
            if request_code == REQUEST_ALBUM_LIST:
                client_soc.sendall(ALBUMS.encode())
            elif request_code == REQUEST_ALBUM_SONGS:
                client_soc.sendall(SONGS_IN_ALBUM.encode())
            elif request_code == REQUEST_SONG_LENGTH:
                client_soc.sendall(LENGTH.encode())
            elif request_code == REQUEST_SONG_LYRICS:
                client_soc.sendall(LYRICS.encode())
            elif request_code == REQUEST_SONG_ALBUM:
                client_soc.sendall(ALBUM_OF_SONG.encode())
            elif request_code == REQUEST_SONG_BY_NAME:
                client_soc.sendall(FIND_SONG_BY_NAME.encode())
            elif request_code == REQUEST_SONG_BY_LYRICS:
                client_soc.sendall(FIND_SONG_BY_LYRICS)
            elif request_code == QUIT:
                server.close()
                client_soc.close()
                break;






if __name__ == "__main__":
    main()