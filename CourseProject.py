"""-----------------------
* Student - Noam Zeira
* Class - Yod'1
* Instructor - Ran Nitzan
* Code - Hangman Game
-------------------------"""


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    Function checks if a certain guess is valid
    Check1 - only alphabet letters; Check2 - char and not a multichar string; Check3 - hasn't been guessed before
    :param letter_guessed: guess to check
    :param old_letters_guessed: list of all the past used letters
    :type old_letters_guessed: list
    :type letter_guessed: string
    :return: True if guess is valid false if not
    :rtype: boolean
    """
    return letter_guessed.isalpha() and len(letter_guessed) == 1 and letter_guessed.lower() not in old_letters_guessed


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    Funcion either updates letter or informs the
    user the letter has already been used/invalid
    depending on the check it performs on the guess.
    :param letter_guessed: guess to check/sort
    :param old_letters_guessed: list of all the past used letters
    :type letter_guessed: string
    :type old_letters_guessed: list
    :return: True if guess has been succesfully added to the list false if it is invalid (unacceptable/already used)
    :rtype: boolean

    """
    # Checking if given guess input is valid
    if check_valid_input(letter_guessed, old_letters_guessed):
        old_letters_guessed += letter_guessed.lower()  # Adding the guessed letter to the guessed letters list
        return True
    else:
        print("X")  # Alerting the user of their invalid input
        # Checking if any letters have already been guessed (if not, there's no need to print the list)
        if old_letters_guessed:
            print(" -> ".join(sorted(old_letters_guessed)))  # Printing a sorted list of the already used letters
        return False


def check_win(secret_word, old_letters_guessed):
    """
    Function checks if used has successfully guessed the secret word
    :param secret_word: secret word to guess
    :param old_letters_guessed: list of already guessed letters
    :type secret_word: string
    :type old_letters_guessed: list
    :return: True if guess is correct False if not
    :rtype: boolean
    """
    guess_correct = True
    # Loop goes over each char in secret word and checks if it is has already been guessed
    for char in secret_word:
        # Checking if char has been guessed
        if char not in old_letters_guessed:
            guess_correct = False  # If char has not been guessed, the user has yet to win
    return guess_correct


def show_hidden_word(secret_word, old_letters_guessed):
    """
    Function creates a string of the secret word that censors the un-guessed letters
    :param secret_word: secret word to guess
    :param old_letters_guessed: a list of the already guessed letters
    :type secret_word: string
    :type old_letters_guessed: list
    :return: hint string
    :rtype: string
    """
    hint_string = ""
    # Loop builds a hint string containing letters for known chars and underlines for unknown chars
    for char in secret_word:
        # Checking if char has been guessed
        if char in old_letters_guessed:
            hint_string += char + " "  # Adding the char to the hint string (and a space for user usability)
        else:
            hint_string += "_ "  # Adding an underline to mark an unknown letter (and a space for user usability)
    return hint_string[:-1]  # Removing the last space and returning the finished hint string


def choose_word(file_path, index):
    """
    Function finds a secret word to guess from a list of words
    :param file_path: path to word file
    :param index: index of secret word
    :type file_path: string
    :type index: int
    :return: tuple of num of unique words and secret word
    :rtype: tuple
    """
    info_tuple = ()
    file = open(file_path, "r")
    file_contents = file.read()
    words_list = file_contents.split(" ")  # Transferring the file words into a list
    return words_list[(index % len(words_list)) - 1]  # Returning the word chosen by the user (index)


def game(file_path, index):
    secret_word = choose_word(file_path, index)  # Variable which contains the secret word
    # Dictionary of all the hangman states:
    HANGMAN_PHOTOS = {0: "    x-------x", 1: "    x-------x\n    |\n    |\n    |\n    |\n    |",
                      2: "    x-------x\n    |       |\n    |       0\n    |\n    |\n    |",
                      3: "    x-------x\n    |       |\n    |       0\n    |       |\n    |\n    |",
                      4: "    x-------x\n    |       |\n    |       0\n    |      /|\\\n    |\n    |",
                      5: "    x-------x\n    |       |\n    |       0\n    |      /|\\\n    |      /\n    |",
                      6: "    x-------x\n    |       |\n    |       0\n    |      /|\\\n    |      / \\\n    |"}
    MAX_TRIES = 6  # Constant defines the maximum amount of incorrect (valid) guesses the user is allowed to make
    fails = 0  # Variable which tracks the number of fails the user has made
    old_letters_guessed = []  # List of all previously guessed letters
    guess = ""  # Variable to contain the user's guess
    print("Let's start!" + "\n" + HANGMAN_PHOTOS[fails] + "\n" + "_ " * len(secret_word))
    while fails < MAX_TRIES and not check_win(secret_word, old_letters_guessed):
        guess = input("Guess a letter: ")
        # Checking if guess is valid and acting by it accordingly
        if try_update_letter_guessed(guess, old_letters_guessed):
            # Valid input, incorrect guess outcome:
            if guess.lower() not in secret_word:
                fails += 1
                print(":(\n" + HANGMAN_PHOTOS[fails] + "\n")  # Printing hangman state
                print(show_hidden_word(secret_word, old_letters_guessed))  # Printing hint
            # Valid input, correct guess outcome:
            else:
                print(show_hidden_word(secret_word, old_letters_guessed))
    # Checking if user has run out of tries and has not won the game
    if fails == MAX_TRIES and not check_win(secret_word, old_letters_guessed):
        print("LOSE")
    else:
        print("WIN")


def main():
    # Printing welcome screen
    print(
        "  _    _                                         \n | |  | |                                        \n | |__| "
        "| __ _ _ __   __ _ _ __ ___   __ _ _ __  \n |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ \n | |  | | (_| "
        "| | | | (_| | | | | | | (_| | | | |\n |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|\n                     "
        " __/ | \n                     |___/\n",
        "6")
    file_path = input("Enter a file path: ")  # Requesting hangman words file
    index = int(input("Enter index: "))  # Requesting index of word in file
    game(file_path, index)  # Starting the game by calling the game function


if __name__ == "__main__":
    main()
    