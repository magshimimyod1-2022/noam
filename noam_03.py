import socket
import unidecode
LISTEN_PORT = 9090
SERVER_ADDR = 'film4me.net'
SERVER_PORT = 92

# Creating constants due to the literals convention:
FRANCE_ERROR = "ERROR#\"France is banned!\""
SUCCESSFUL = "MOVIDEDATA"
PROBLEMATIC = "SERVERERROR"
UNSUCCESSFUL = "ERROR"
FORBIDDEN_COUNTRY = "France"
JPG_SUFFIX = "jpg"
DOT = "."
HASH = "#"


def main():

    while True:
        # Proxy server part:

        france_detected = False
        server_side = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Binding to local port 9090
        server_side.bind(('', LISTEN_PORT))
        # Listen for incoming connections
        server_side.listen(1)
        client_soc, client_addr = server_side.accept()
        client_msg = client_soc.recv(1024)
        client_msg = client_msg.decode()

        # Banning france
        if FORBIDDEN_COUNTRY in client_msg or FORBIDDEN_COUNTRY.lower() in client_msg:
            france_detected = True

        # Proxy client part:

        client_side = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (SERVER_ADDR, SERVER_PORT)
        client_side.connect(server_address)
        client_side.sendall(client_msg.encode())
        msg = client_side.recv(1024)
        msg = msg.decode()

        # Response bug fixing and enhancing:

        response_type = msg.split(HASH)[0].split(DOT)[0]
        if response_type == PROBLEMATIC:
            msg = msg.replace(msg.split(HASH)[0], UNSUCCESSFUL)  # Replacing the incorrect error prefix
        if response_type == SUCCESSFUL:
            msg = msg.replace(JPG_SUFFIX, DOT + JPG_SUFFIX)
            msg = unidecode.unidecode(msg)  # Normalizing the foreign letters into regular ascii chars
        if france_detected:
            msg = FRANCE_ERROR  # Changing the response into a france denial error
        client_soc.sendall(msg.encode())

        server_side.close()
        client_side.close()
        client_soc.close()


if __name__ == "__main__":
    main()
